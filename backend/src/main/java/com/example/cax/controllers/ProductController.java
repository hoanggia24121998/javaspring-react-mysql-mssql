package com.example.cax.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.cax.model.test.Product;
import com.example.cax.repository.test.ProductRepository;

@RestController
@RequestMapping("product")
@CrossOrigin
public class ProductController {
	@Autowired
	private ProductRepository repo;

	@GetMapping
	public ResponseEntity<?> getProducts() {
		try {
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
			List<Product> data = repo.findAll();
	        headers.add("X-Total-Count", String.valueOf(data.size()));
			headers.add("Access-Control-Expose-Headers","X-Total-Count");
	        return new ResponseEntity<List<Product>>(data, headers, HttpStatus.OK);	
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}

	@GetMapping("/add")
	public ResponseEntity<?> addNew() {
		try {
			return ResponseEntity.ok().body(repo.save(new Product("Lifeboy")));
		} catch (Exception e) {
			return ResponseEntity.internalServerError().body(e.getMessage());
		}
	}
}
