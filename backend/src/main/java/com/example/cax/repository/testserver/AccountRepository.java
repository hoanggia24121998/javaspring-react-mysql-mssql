package com.example.cax.repository.testserver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.cax.model.testserver.Account;

public interface AccountRepository extends JpaRepository<Account, String> {
	List<Account> findAll();
}
