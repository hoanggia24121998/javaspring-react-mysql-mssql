package com.example.cax.repository.test;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.cax.model.test.Product;

public interface ProductRepository extends JpaRepository<Product, Long>{
	List<Product> findAll();
}
