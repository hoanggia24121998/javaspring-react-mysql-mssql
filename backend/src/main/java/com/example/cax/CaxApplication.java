package com.example.cax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaxApplication {
	public static void main(String[] args) {
		SpringApplication.run(CaxApplication.class, args);
	}
}
