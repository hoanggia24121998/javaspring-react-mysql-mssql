package com.example.cax.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.example.cax.repository.test", entityManagerFactoryRef = "testEntityManagerFactory", transactionManagerRef = "testTransactionManager")
public class TestDataSourceConfig {
	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.test")
	public DataSourceProperties testDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean(name = "mysqlDataSource")
	@Primary
	public DataSource testDataSource() {
		return testDataSourceProperties().initializeDataSourceBuilder().type(DriverManagerDataSource.class).build();
	}

	@Bean(name = "testEntityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean testserverEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		final HashMap<String, Object> hibernateProperties = new HashMap<String, Object>();
		hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
		return builder.dataSource(testDataSource()).packages("com.example.cax.model.test")
				.properties(hibernateProperties).build();
	}

	@Bean(name = "testTransactionManager")
	@Primary
	public PlatformTransactionManager testTransactionManager(
			final @Qualifier("testEntityManagerFactory") LocalContainerEntityManagerFactoryBean testEntityManagerFactory) {
		return new JpaTransactionManager(testEntityManagerFactory.getObject());
	}
}
