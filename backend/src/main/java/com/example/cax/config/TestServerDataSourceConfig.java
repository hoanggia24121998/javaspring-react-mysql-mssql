package com.example.cax.config;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(basePackages = "com.example.cax.repository.testserver", entityManagerFactoryRef = "testserverEntityManagerFactory", transactionManagerRef = "testserverTransactionManager")
public class TestServerDataSourceConfig {
	@Bean
	@ConfigurationProperties("spring.datasource.testserver")
	public DataSourceProperties testserverDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	public DataSource testserverDataSource() {
		return testserverDataSourceProperties().initializeDataSourceBuilder().type(DriverManagerDataSource.class)
				.build();
	}

	@Bean(name = "testserverEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean testserverEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		final HashMap<String, Object> hibernateProperties = new HashMap<String, Object>();
		hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2012Dialect");
		return builder.dataSource(testserverDataSource()).packages("com.example.cax.model.testserver")
				.properties(hibernateProperties)
				.build();
	}

	@Bean(name = "testserverTransactionManager")
	public PlatformTransactionManager testserverTransactionManager(
			final @Qualifier("testserverEntityManagerFactory") LocalContainerEntityManagerFactoryBean testserverEntityManagerFactory) {
		return new JpaTransactionManager(testserverEntityManagerFactory.getObject());
	}
}
