package com.example.cax;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.cax.services.BMICalculator;

// @SpringBootTest
class CaxApplicationTests {

	@Test
	void should_ThrowArithmeticException_When_HeightZero() {
		// give
		double weight = 50.0;
		double height = 0;

		// when
		Executable excutable = new Executable() {
			@Override
			public void execute() throws Throwable {
				BMICalculator.isDietRecommended(weight, height);
			}
		};

		// then
		// Executable excutable = () -> BMICalculator.isDietRecommended(weight, height);
		assertThrows(ArithmeticException.class, excutable);
	}

}
