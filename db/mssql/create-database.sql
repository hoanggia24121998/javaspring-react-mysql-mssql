CREATE DATABASE [testserver]
GO

USE [testserver];
GO

CREATE TABLE account (
    id INT NOT NULL IDENTITY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL
    PRIMARY KEY (id)
);
GO

SET IDENTITY_INSERT account ON;
GO

INSERT INTO [account] (id, username, password, name)
VALUES 
(1, 'gianh', 'gianh-pass', 'Hoang Gia'),
(2, 'gianh2', 'gianh-pass', 'Hoang Gia 2'); 
GO