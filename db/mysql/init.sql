CREATE DATABASE test;
USE test;

CREATE TABLE Product (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(255),
    price double,
    quantity double
);