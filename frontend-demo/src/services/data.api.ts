import { Users } from "../types/Users.type";
import { Products } from "../types/Products.type";
import http from "../utils/http";

export const getUsers = () => http.get<Users>("users");

export const getProducts = () => http.get<Products>("product");
