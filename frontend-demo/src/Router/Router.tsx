import { useRoutes } from "react-router-dom";
import Users from "../pages/Users";
import Products from "../pages/Products";
import NotFound from "../pages/NotFound";

export default function Router() {
  return useRoutes([
    {
      path: "/",
      element: <Users />,
    },
    { path: "/products", element: <Products /> },
    { path: "*", element: <NotFound /> },
  ]);
}
