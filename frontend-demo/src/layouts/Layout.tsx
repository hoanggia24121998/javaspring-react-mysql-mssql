/* eslint-disable @typescript-eslint/no-explicit-any */
import Breadcrumb from "../components/Breadcrumb";
import Table from "../components/Table";
import Navbar from "../components/Navbar";
import { ReactNode } from "react";

type Props = {
  title?: string;
  tbHeader?: string[];
  tbData?: any[];
  children?: ReactNode;
};

const Layout = ({ title, tbHeader, tbData, children }: Props) => {
  return (
    <>
      <Navbar />;
      <section className="section">
        {children ? (
          children
        ) : (
          <>
            <Breadcrumb title={title} />
            <Table headers={tbHeader} data={tbData} />
          </>
        )}
      </section>
    </>
  );
};

export default Layout;
