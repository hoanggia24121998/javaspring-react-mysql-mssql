import { useQuery } from "@tanstack/react-query";
import Layout from "../layouts/Layout";
import { getProducts } from "../services/data.api";

const tbHeader = ["#", "ID", "Name", "Quantity", "Price"];

const Products = () => {
  const { data, isLoading } = useQuery({
    queryKey: ["products"],
    queryFn: getProducts,
  });
  return (
    <>
      {isLoading ? (
        <div>Loading....</div>
      ) : (
        <Layout title="Product" tbHeader={tbHeader} tbData={data?.data} />
      )}
    </>
  );
};

export default Products;
