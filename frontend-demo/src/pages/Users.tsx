import { useQuery } from "@tanstack/react-query";
import Layout from "../layouts/Layout";
import { getUsers } from "../services/data.api";

const tbHeader = ["#", "ID", "Username", "Password", "Name"];

const Users = () => {
  const { data: dataRaw, isLoading } = useQuery({
    queryKey: ["users"],
    queryFn: getUsers,
  });
  return (
    <>
      {isLoading ? (
        <div>Loading....</div>
      ) : (
        <Layout title="Users" tbHeader={tbHeader} tbData={dataRaw?.data} />
      )}
    </>
  );
};

export default Users;
