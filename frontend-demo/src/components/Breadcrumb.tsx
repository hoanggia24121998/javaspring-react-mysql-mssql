type Props = {
  title?: string;
};
const Breadcrumb = ({ title }: Props) => {
  return (
    <nav className="breadcrumb" aria-label="breadcrumbs">
      <ul>
        <li>
          <a href="#">Bulma</a>
        </li>
        <li className="is-active">
          <a href="#" aria-current="page">
            {title}
          </a>
        </li>
      </ul>
    </nav>
  );
};

export default Breadcrumb;
