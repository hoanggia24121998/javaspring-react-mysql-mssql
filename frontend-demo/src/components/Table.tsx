/* eslint-disable @typescript-eslint/no-explicit-any */
type Props = {
  headers?: string[];
  data?: any[];
};
const Table = ({ headers, data }: Props) => {
  console.log(data);
  return (
    <table className="table is-bordered is-fullwidth ">
      <thead>
        <tr>{headers && headers.map((col) => <th key={col}>{col}</th>)}</tr>
      </thead>
      <tbody>
        {data &&
          data.map((item, i) => {
            return (
              <tr>
                <th>{i + 1}</th>
                {Object.keys(item).map((value) => {
                  return <td>{item[value]}</td>;
                })}
              </tr>
            );
          })}
      </tbody>
    </table>
  );
};

export default Table;
