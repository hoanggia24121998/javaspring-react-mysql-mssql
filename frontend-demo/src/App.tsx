import "bulma/css/bulma.min.css";
import { BrowserRouter } from "react-router-dom";
import Router from "./Router/Router";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Router />
      </div>
    </BrowserRouter>
  );
}

export default App;
