export interface User {
  id: number;
  username: string;
  password: string;
  name: string;
}

export type Users = User[];
