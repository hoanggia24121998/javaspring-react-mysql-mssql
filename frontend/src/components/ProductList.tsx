import { List, Datagrid, TextField } from "react-admin";

const ProductList = (props: any) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="name" />
        <TextField source="quantity" />
        <TextField source="price" />
      </Datagrid>
    </List>
  );
};

export default ProductList;
