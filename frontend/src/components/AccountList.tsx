import { List, Datagrid, TextField } from "react-admin";

const AccountList = (props: any) => {
  return (
    <List {...props}>
      <Datagrid>
        <TextField source="id" />
        <TextField source="username" />
        <TextField source="password" />
        <TextField source="name" />
      </Datagrid>
    </List>
  );
};

export default AccountList;
